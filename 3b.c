#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IDLEN 25
#define IDs 1300
struct rect {
	int ID;
	int xOffset;
	int yOffset;
	int height;
	int width;
};

struct matches {
	int x;
	int y;
};

struct matches mtch[250000];

struct rect rects[IDs];

int main() {
	FILE* f = fopen("input3", "r");
	int i = 0;
	const char *IDS[1300];
	char str[IDLEN];
	while(fgets(str, IDLEN, f) != NULL) {
		//IDS[iter] = strndup(str, IDLEN);
		sscanf(str, "#%d @ %d,%d: %dx%d", &rects[i].ID, 
				&rects[i].xOffset, &rects[i].yOffset, 
				&rects[i].width, &rects[i].height);
		i += 1;
	}
	printf("%d", i); 
	int counter = 0;
	int match = 0;
	for(int j = 0; j<(i); j++) {
		for(int jj=0; jj<i; jj++) {
			if(j == jj) continue;
			if(abs(rects[j].xOffset -rects[jj].xOffset)> 30)
				continue;
			if(abs(rects[j].yOffset -rects[jj].yOffset)> 30)
				continue;


			for(int x = rects[j].xOffset+1; x<=rects[j].xOffset + rects[j].width; x++){
				for(int y = rects[j].yOffset+1; y<=rects[j].yOffset + rects[j].height; y++){		
					for(int xx = rects[jj].xOffset+1; xx<=rects[jj].xOffset + rects[jj].width; xx++){
						for(int yy = rects[jj].yOffset+1; yy<=rects[jj].yOffset + rects[jj].height; yy++){
							if(x == xx && y == yy){
								match = 0;
								for(int k = 0; k<counter; k++){
									if(mtch[k].x == x && mtch[k].y == y){
										match = 1;
										break;

									}
								}
								if(!match) {

									mtch[counter].x = x;
									mtch[counter].y = y;	
									counter+=1;

								}	

								if(match) break;
							}
						}
					}
				}
			}
		}
		printf("\r                                ");
		printf("\rprogress: %d, cnt: %d", j, counter);
		fflush(stdout);
	}		
	printf("\ncount: %d\n", counter);


	for(int j = 0; j<(i); j++) {
		int match = 0;
		for(int k = 0; k<counter; k++) {
			for(int x = rects[j].xOffset+1; x<=rects[j].xOffset + rects[j].width; x++){
				for(int y = rects[j].yOffset+1; y<=rects[j].yOffset + rects[j].height; y++){		
					if(mtch[k].x == x && mtch[k].y == y) {
						match = 1;
						break;
					}
				}
				if(match) break;
			}
			if(match) break;
		}
		if(!match){
			printf("ID: %d\n", j);
			//printf("x: %d, y: %d\n", x, y);
			exit(0);
		}
		match = 0;

	}
	exit(0);
}
