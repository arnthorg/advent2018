#include <stdio.h>
#include <stdlib.h>

int main() {
	FILE* f = fopen("input", "r");

	if(!f) {
		printf("file can't be read");
		exit(-1);
	}
	int num=0;
	long sum=0;
	while(fscanf(f, "%d", &num) != EOF ) {
		sum += num;
	}
	printf("The sum is: %ld\n", sum);
	return 0;
}
