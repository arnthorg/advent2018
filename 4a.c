#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRLEN 45
#define ENTRIES 1200
struct entry{
	int ID;
	int M;
	int d;
	int h;
	int m;
	char str[25]; 
};
struct entry entries[ENTRIES];
int main() {
	FILE* f = fopen("input4", "r");
	int i = 0;
	char str[STRLEN];
	while(fgets(str, STRLEN, f) != NULL) {
		//IDS[iter] = strndup(str, IDLEN);
		sscanf(str, "[1518-%d-%d %d:%d] %25c", &entries[i].M, &entries[i].d, &entries[i].h, &entries[i].m, entries[i].str);
		i += 1;
	}
	for(int j=0; j<i; j++) {
		printf("%s", entries[j].str);
	}
	exit(0);
}
