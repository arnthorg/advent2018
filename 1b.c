#include <stdio.h>
#include <stdlib.h>

_Bool findDuplicate(int *arr, int arrlen,  int num) {
	for(int i = 0; i<arrlen; i++) {
		if(num == arr[i]) {
			return 1;
		}
	}
	return 0;
}

int main() {
	FILE* f = fopen("input", "r");

	if(!f) {
		printf("file can't be read\n");
		exit(-1);
	}
	int num=0;
	long sum=0;
	int frequency[150100];
	frequency[0] = 0;
	unsigned int freqlen = 1;
	for(int i =0; i<150; i++){	
		while(fscanf(f, "%d", &num) != EOF ) {
			sum += num;
			if(findDuplicate(frequency, freqlen, sum)){
				printf("duplicate found: %ld\n", sum);
				exit(0);
			}
			else {
				frequency[freqlen++] = sum;
			}
		}
		fseek(f, 0, SEEK_SET);
	}
	printf("length: %d\n", freqlen);
	exit(0);
	return 0;
}
