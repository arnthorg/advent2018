#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

unsigned int countInstances(char *arr, int strLen, char letter){
	unsigned int count = 0;	
	for(int i=0; i<strLen; i++) {
		if(arr[i] == letter)
			count += 1;
	}
	return count;
}

#define MAX 500
int main() {
	FILE* f = fopen("input2", "r");
	char str[MAX];
	bool duplicates[MAX][3] = {false};
	int iter = 0;
	while(fgets(str, MAX, f) != NULL) {
		unsigned int strLen = strlen(str);
		for(int i = 0; i < strLen; i++){
			if(countInstances(str, strLen, str[i]) ==3){
				duplicates[iter][1] = true;
			}
			else if(countInstances(str, strLen, str[i]) ==2)
				duplicates[iter][0] = true;	  

			if(duplicates[iter][0] && duplicates[iter][1]) break;
		}
		iter += 1;
	}
	int twoCount= 0;
	int threeCount = 0;
	for(int i = 0; i < iter; i++){
		if(duplicates[i][0])
			twoCount++;
		if(duplicates[i][1])
			threeCount++;

	}
	printf("two: %d, three: %d, product: %d\n", twoCount, threeCount, twoCount*threeCount);
	exit(0);
}
