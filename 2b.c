#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IDLENGTH 5
#define MAX 500
#define IDLEN 25+1
int main() {
	FILE* f = fopen("input2", "r");
	char str[MAX];
	int iter = 0;
	const char *IDS[255]; 
	while(fgets(str, MAX, f) != NULL) {
		IDS[iter] = strndup(str, IDLEN);

		iter += 1;
	}

	for(int i = 0; i<iter; i++){
		for(int ii = 0; ii<iter; ii++){
			if(i == ii) continue;
			int differences = 0;
			for(int j=0; j<IDLEN; j++){
				if(IDS[i][j] != IDS[ii][j])
				differences +=1;
			
			}
			if(differences == 1) {
				printf("%s\n%s\n\n", IDS[i], IDS[ii]);	
				exit(0);
			}
		}
	}

	exit(0);
}
